import React from 'react';
//import OurFlatList from './app/components/ourFlatList/OurFlatList.js'
import ConexionFetch from './app/components/conexionFetch/ConexionFetch'
import {
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});

const App = () => {
  return (
      <ConexionFetch />
  );
}

export default App;
